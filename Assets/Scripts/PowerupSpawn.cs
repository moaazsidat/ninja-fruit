﻿using UnityEngine;
using System.Collections;

public class PowerupSpawn : MonoBehaviour {
	float speed; // for powerup speed
	
	// Use this for initialization
	void Start() {
		speed = 4f; //set speed
	}

	void Update() {
		
		// Get powerup current position
		Vector2 position = transform.position;
		
		//Compute the powerup new position
		position = new Vector2 (position.x, position.y - speed * Time.deltaTime);
		
		// Update the powerup position
		transform.position = position;
		
		// Bottom left of the screen
		Vector2 min = Camera.main.ViewportToWorldPoint (new Vector2 (0, 0));
		
		// If powerup went outside the bottom of the screen, destroy powerup
		if (transform.position.y < min.y) {
			Destroy(gameObject);
		}
	}
}
