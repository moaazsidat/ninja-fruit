﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIManager : MonoBehaviour {
	public AudioManager am;
	public bool gameOver;

	void Start () {
		gameOver = false;
	}
	
	void Update () {	
	}

	public void Pause() {
		if (Time.timeScale == 1) {
			Time.timeScale = 0;
		} else if (Time.timeScale == 0)  {
			Time.timeScale = 1;
		}
	}

	public void gameOverActivated() {
		gameOver = true;

	}

	public void Play() {
		gameOver = false;
		Time.timeScale = 1;
		Application.LoadLevel("Good_Watermelon");
	}

	public void Rules() {
		gameOver = false;
		Time.timeScale = 1;
		Application.LoadLevel("Instructions");
	}

	public void Replay() {
		gameOver = false;
		Time.timeScale = 1;
		Application.LoadLevel(Application.loadedLevel); 
	}

	public void Menu() {
		Pause();
		Application.LoadLevel("Game_Menu");
	}

	public bool getGameOver() {
		return gameOver;
	
	}
}
