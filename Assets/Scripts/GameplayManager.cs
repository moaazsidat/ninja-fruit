﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameplayManager : MonoBehaviour {
	public Text scoreText; 
	private int score;
	public Text gameOverHeading;
	public Button[] buttons;
	public UIManager uiManager;
	public AudioManager am;
	public bool hasInvinciblePowerup = false;
	public bool hasMiniPowerup = false;

	void Start () {
		score = 0;

		// Prevent game over heading from showing at start
		gameOverHeading.enabled = false;

		// prevent buttons from showing mid game
		foreach (Button button in buttons) {
			button.gameObject.SetActive(false);
		}
	}
	
	// Update is called once per frame
	void Update () {
		scoreText.text = "Score: " + score;
		if (uiManager.getGameOver()) {
			// show game over heading
			gameOverHeading.enabled = true;
			
			// show buttons when game ends
			foreach (Button button in buttons) {
				button.gameObject.SetActive(true);
			}
		}
	}
	
	public void incrementScore() {
		if (!uiManager.getGameOver()) {
			score += 100;
		}
	}
	public int getScore() {
		return score;
	}

	public void test() {
		print("GameplayManager Test");
	}
	public void toggleInviciblePowerup() {
		hasInvinciblePowerup = !hasInvinciblePowerup;

	}
	public void toggleMiniPowerup() {
		hasMiniPowerup = !hasMiniPowerup;
	}
}