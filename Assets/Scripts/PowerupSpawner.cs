﻿using UnityEngine;
using System.Collections;
public class PowerupSpawner : MonoBehaviour {
	public GameObject[] powerups; //This is the powerup prefab
	public UIManager uiManager;
	void Start() {
		float spawnInNSeconds;
		spawnInNSeconds = Random.Range(5f, 7f);

		Invoke("SpawnPowerup", spawnInNSeconds);
	}
	void Update() {	
	}
	
	//Function to spawn an enemy
	void SpawnPowerup() {
		// this is the bottom left point of the screen
		Vector2 min = Camera.main.ViewportToWorldPoint (new Vector2 (0, 0));
		
		// this is the top right point of the screen
		Vector2 max = Camera.main.ViewportToWorldPoint (new Vector2 (1, 1));
		
		// instantiate a powerup
		GameObject aPowerup = (GameObject)Instantiate(powerups[Random.Range(0, powerups.Length)]);
		aPowerup.transform.position = new Vector2 (Random.Range (min.x, max.x), max.y);
		
		//Schedule when to spawn next powerup
		ScheduleNextPowerupSpawn();
	}
	
	void ScheduleNextPowerupSpawn() {
		float spawnInNSeconds;
		spawnInNSeconds = Random.Range(5f, 7f);
		
		if (!uiManager.getGameOver()) {
			Invoke("SpawnPowerup", spawnInNSeconds);
		} else {
			CancelInvoke("SpawnPowerup");
		}
		
	}
}
