﻿using UnityEngine;
using System.Collections;

public class WatermelonScript : MonoBehaviour {
	public float speed = 2; // speed in meters per second
	public float minPost = -9.0f;
	public float maxPos = 9.9f; 
	Vector3 position;
	public UIManager uiManager;
	public AudioManager am;
	private GameObject canvas;
	private GameplayManager gameplayManager;
	private bool blinkOn = true;
	private Vector3 originalScale; 
	private Renderer playerRenderer;
	private Animator animator;

	void Start () { 
		am.backgroundMusic.Play (); 
		position = transform.position;
		originalScale = transform.localScale;
		
		// initialize canvas to the game's canvas
		canvas = GameObject.Find("Canvas");

		// fetch gameplayManager from the canvas 
		gameplayManager = canvas.GetComponent<GameplayManager>();
		
		// fetch renderer for the player
		playerRenderer = gameObject.GetComponent<Renderer>();

		// fetch animator for the player
		animator = GetComponent<Animator>();	
	}

	void OnCollisionEnter2D(Collision2D other) {
		if (other.collider.tag == "EnemySword") {
			if (gameplayManager.hasInvinciblePowerup) {
				Destroy (other.gameObject);
				gameplayManager.incrementScore();
				return;
			}
			Destroy (other.gameObject);
			animator.SetBool("isWalkingRight", false);
			animator.SetBool("isWalkingLeft", false);
			animator.SetBool("isStruck", true);
			
			uiManager.gameOverActivated();
			am.backgroundMusic.Stop();
			am.collisionSound.Play();
		}

		if (other.collider.tag == "InvinciblePowerup") {
			Destroy (other.gameObject);
			am.redGem.Play();


			if (!gameplayManager.hasInvinciblePowerup) {
				gameplayManager.toggleInviciblePowerup();
				InvokeRepeating("startBlink", 0f, 0.2f);
				Invoke("CancelBlink", 5f);
			}
		}

		if (other.collider.tag == "MiniPowerup") {
			Destroy (other.gameObject);
			am.blueGem.Play();

			if (!gameplayManager.hasMiniPowerup) {
				gameplayManager.toggleMiniPowerup();
				Invoke("reduceSize", 0f);
				Invoke("resetSize", 5f);
			}
		}
	}
	
	void Update () {
		if (!uiManager.gameOver) {
			animator.SetBool("isStruck", false);
			if (Input.GetKey(KeyCode.RightArrow)) {
			animator.SetBool("isWalkingRight", true);
			animator.SetBool("isWalkingLeft", false);
		} else if (Input.GetKey(KeyCode.LeftArrow)) {
			animator.SetBool("isWalkingRight", false);
			animator.SetBool("isWalkingLeft", true);
		} else {
			animator.SetBool("isWalkingRight", false);
			animator.SetBool("isWalkingLeft", false);
		}
			position.x += Input.GetAxis("Horizontal") * speed * Time.deltaTime;
			position.x = Mathf.Clamp(position.x, minPost, maxPos);

			transform.position = position;
		}
	}
	void startBlink() {
		if (blinkOn) {
			playerRenderer.enabled = false;
			blinkOn = false;
		} else {
			playerRenderer.enabled = true;
			blinkOn = true;
		}
	}

	void CancelBlink() {
		CancelInvoke("startBlink");
		gameplayManager.toggleInviciblePowerup();
		playerRenderer.enabled = true;
		blinkOn = true;
	}

	void reduceSize() {
		Vector3 miniSize = new Vector3(0.4f, 0.4f, 0.4f);
		transform.localScale -= miniSize;
	}
	void resetSize() {
		gameplayManager.toggleMiniPowerup();
		transform.localScale = originalScale;
	}
}

	