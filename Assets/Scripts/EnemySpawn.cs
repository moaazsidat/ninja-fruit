﻿using UnityEngine;
using System.Collections;

public class EnemySpawn : MonoBehaviour {
	public GameObject sword; //This is the enemy prefab
	public UIManager uiManager;
	float maxSpawnRateInSeconds = 1f;
	void Start() {
		Invoke("SpawnEnemy", maxSpawnRateInSeconds);	
		
		//increase spawn rate every 30 seconds
		InvokeRepeating("IncreaseSpawnRate", 1f, 15f);
	}
	void Update() {	
	}
	
	//Function to spawn an enemy
	void SpawnEnemy() {
		//this is the bottom left point of the screen
		Vector2 min = Camera.main.ViewportToWorldPoint (new Vector2 (0, 0));
		
		//this is the top right point of the screen
		Vector2 max = Camera.main.ViewportToWorldPoint (new Vector2 (1, 1));
		
		//instantiate an enemy
		GameObject anEnemy = (GameObject)Instantiate(sword);
		anEnemy.transform.position = new Vector2 (Random.Range (min.x, max.x), max.y);
		
		//Schedule when to spawn next enemy
		ScheduleNextEnemySpawn();
	}
	
	void ScheduleNextEnemySpawn() {
		
		float spawnInNSeconds;
		if (maxSpawnRateInSeconds > 1f) {
			//pick a number between 1 and maxSpawnRateInSeconds
			spawnInNSeconds = Random.Range(1f, maxSpawnRateInSeconds);
		} else {
			spawnInNSeconds = 1f;
		}
		if (!uiManager.getGameOver()) {
			Invoke("SpawnEnemy", spawnInNSeconds);
		} else {
			CancelInvoke("SpawnEnemy");
		}
	}
	
	//Function to increase difficulty of game
	void IncreaseSpawnRate() {
		if (maxSpawnRateInSeconds > 1f) {
			maxSpawnRateInSeconds--;
		}
		
		if (maxSpawnRateInSeconds == 1f) {
			CancelInvoke("IncreaseSpawnRate");
		}	
	} 
	
}