﻿using UnityEngine;
using System.Collections;

public class SwordSpawn : MonoBehaviour {
	private GameObject canvas;
	private GameplayManager gameplayManager;
	float speed; // for the enemy speed
	
	// Use this for initialization
	void Start() {
		// initialize canvas to the game's canvas
		canvas = GameObject.Find("Canvas");

		// fetch gameplayManager from the canvas 
		gameplayManager = canvas.GetComponent<GameplayManager>();

		// set speed
		setSpeed();
	}
	
	// Update is called once per frame
	void Update() {
		renderer.enabled = true;
		
		//Get enemy current position
		Vector2 position = transform.position;
		
		//Compute the enemy new position
		position = new Vector2 (position.x, position.y - speed * Time.deltaTime);
		
		//Update the enemy position
		transform.position = position;
		
		//This is the bottom left of the screen
		Vector2 min = Camera.main.ViewportToWorldPoint (new Vector2 (0, 0));
		
		//If enemy went outside the bottom of the screen, destroy enemy
		if (transform.position.y < min.y) {
			// incrementScore when sword hits ground
			gameplayManager.incrementScore();

			Destroy(gameObject);
		}
	}
	void setSpeed() {
		speed = 7f;
		if (gameplayManager.getScore() > 1000) {
			speed = 8f;
		}
		if (gameplayManager.getScore() > 1500) {
			speed = 9f;
		}
		if (gameplayManager.getScore() > 2500) {
			speed = 10f;
		}
		if (gameplayManager.getScore() > 3500) {
			speed = 11f;
		}
		if (gameplayManager.getScore() > 4500) {
			speed = 12f;
		}
		if (gameplayManager.getScore() > 6000) {
			speed = 13f;
		}
		if (gameplayManager.getScore() > 8000) {
			speed = 14f;
		}
		if (gameplayManager.getScore() > 10000) {
			speed = 15f;
		}
	}
}